/**
 * Created by UdaySravanK on 6/29/17.
 */
'use strict';

var express = require('express');
var app = express();
var port = process.env.PORT || 3008;

app.get('/', function (req, res) {
  res.send('Basic setup is in progress');
});

app.listen(port, function() {
  console.log('Express server listening on port ' + port);
  console.log('env = ' + app.get('env') +
    '\n__dirname = ' + __dirname +
    '\nprocess.cwd = ' + process.cwd());
});